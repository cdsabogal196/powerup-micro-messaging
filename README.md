<br />
<div align="center">
<h3 align="center">PRAGMA POWER-UP</h3>
  <p align="center">
    In this challenge you are going to design the backend of a system that centralizes the services and orders of a restaurant chain that has different branches in the city.
  </p>
</div>

### Built With

* ![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
* ![Spring](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
* ![Gradle](https://img.shields.io/badge/Gradle-02303A.svg?style=for-the-badge&logo=Gradle&logoColor=white)
* ![MySQL](https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white)


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these steps.

### Prerequisites

* JDK 17 [https://jdk.java.net/java-se-ri/17](https://jdk.java.net/java-se-ri/17)
* Gradle [https://gradle.org/install/](https://gradle.org/install/)
* MySQL [https://dev.mysql.com/downloads/installer/](https://dev.mysql.com/downloads/installer/)

### Recommended Tools
* IntelliJ Community [https://www.jetbrains.com/idea/download/](https://www.jetbrains.com/idea/download/)
* Postman [https://www.postman.com/downloads/](https://www.postman.com/downloads/)

### Installation

1. Clone the repository
2. Change directory
   ```sh
   cd power-up-arquetipo-v3
   ```
3. Create a new database in MySQL called powerup
4. Update the database connection settings
   ```yml
   # src/main/resources/application-dev.yml
   spring:
      datasource:
          url: jdbc:mysql://localhost/powerup
          username: root
          password: <your-password>
   ```
5. After the tables are created execute src/main/resources/data.sql content to populate the database
6. Open Swagger UI and search the /auth/login endpoint and login with userDni: 123, password: 1234

<!-- USAGE -->
## Usage

1. Right-click the class PowerUpApplication and choose Run
2. Open [http://localhost:8090/swagger-ui/index.html](http://localhost:8090/swagger-ui/index.html) in your web browser

## Tests

- Right-click the test folder and choose Run tests with coverage
### USE ENPOINT /cellphone/

This is the README file for the project. Below, you will find instructions on how to use the API and access the endpoints using Swagger.

## Step 1: Accessing the API

Once you are in the environment where the API is located, you can access it using the corresponding URL.

## Step 2: Accessing Swagger

To interact with the API, go to the Swagger link. From there, you will be able to see all the available endpoints and test them.

http://localhost:8095/swagger-ui/index.html

# US 14 NOTIFY THAT IT'S READY
This project focuses on providing the necessary functionality for a restaurant employee to mark an order as delivered. By marking an order as delivered, the order cycle is completed, allowing the employee to focus on other orders.

# Functional Requirements
1. Only orders in the "Ready" state can be marked as delivered.
2. No order in the "Delivered" state can be modified to change its state to anything other than "Ready".
3. To change the state to "Delivered", the employee must enter the security PIN provided to the customer to claim their order. These orders will be generated in the Plazoleta microservice.

# Usage Instructions
1. Step 1: Download the microservices
Download the following microservices from the following repositories:
"https://gitlab.com/cdsabogal196/powerup-micro-users"
 "https://gitlab.com/cdsabogal196/powerup-micro-plazoleta" 
 "https://gitlab.com/cdsabogal196/powerup-micro-traceability" 
# Step 2: Access the microservice -- /cellphone/customer-notification

Log in to the "Users" microservice using the user with the "ROLE_EMPLOYEE" role.
Make sure you have orders generated in the "Plazoleta" microservice so you can mark them as delivered.

input endpoint: "/cellphone/customer-notification"

Ingresa en el campo Swager el id del pedido como mail y password correspondiente del cliente
 ```yml´
  public void callPhoneNotification(String idOrder, String mail, String password)
  ```
Verify that both fields are from a valid customer order and the customer will be notified that the process is over and they can pick up their order by sending them a ping.
 At the end, a 4-number pin will be sent a sms with which the client can receive the food.
# Step 3: Use Twilio to generate SMS
This project uses Twilio to generate the text messages (SMS) with the security pins for customers to claim their orders.
