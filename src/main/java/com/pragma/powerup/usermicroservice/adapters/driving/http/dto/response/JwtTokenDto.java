package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response;

import java.util.List;

public class JwtTokenDto {
    private String sub;
    private long iat;
    private long exp;

    // Getters y setters para los campos
    // ...
}