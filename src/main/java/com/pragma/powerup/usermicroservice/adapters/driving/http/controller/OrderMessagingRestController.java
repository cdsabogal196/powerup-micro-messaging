package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IOrderMessagingHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cellphone")
@RequiredArgsConstructor
public class OrderMessagingRestController {
    @Autowired
    private IOrderMessagingHandler pedidoHandler;


    @PostMapping("/customer-notification")
    public ResponseEntity<String> callNotification(String idOrder, String mail, String password) {
        if (idOrder == null || mail == null || password == null) {
            return ResponseEntity.badRequest().body("Missing parameters");
        }

        try {
            pedidoHandler.callPhoneNotification(idOrder, mail, password);
            return ResponseEntity.ok("Notification sent successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Something went wrong while sending the notification.");
        }
    }


}
