package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IOrderMessagingHandler;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
@RequiredArgsConstructor
public class OrderMessagingImpl implements IOrderMessagingHandler {

    public static final String ACCOUNT_SID = "ACff989d5a073c11a7178f0eb1f14f688d";
    public static final String AUTH_TOKEN = "7547498f7d471f1ad0211fd69f6aa53f";


    public void callPhoneNotification(String idOrder, String mail, String password) {

        String endpointUrl = "http://localhost:8090/pedido/notificarPedidoListo?idPedido=" + idOrder + "&mail=" + mail + "&password=" + password;

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(endpointUrl))
                .header("Content-Type", "application/json")
                .GET()
                .build();

        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            if(response.statusCode() == 500 || response.body().contains("Wrong credentials")){
                throw new Exception("Something was wrong during http request, the request is wrong or the credentials are invalid");
            }

            String cellPhone = response.body();
            System.out.println(cellPhone);
            String[] parts = cellPhone.split("/");
            String cellphone = parts[0];
            String pin = parts[1];

            Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
            Message message = Message.creator(
                            new com.twilio.type.PhoneNumber(cellphone),
                            new com.twilio.type.PhoneNumber("+13614365529"),
                            "su pedido esta listo su pin para recoger su orden es : " + pin)
                    .create();

            System.out.println(message.getSid());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}

