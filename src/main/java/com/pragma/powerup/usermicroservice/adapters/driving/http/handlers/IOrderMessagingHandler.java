
package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;


public interface IOrderMessagingHandler {

    void callPhoneNotification(String idOrder, String mail, String password);

}
