package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IOrderMessagingHandler;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class OrderMessagingRestControllerTest {


    @Mock
    private IOrderMessagingHandler pedidoHandler;

    @InjectMocks
    private OrderMessagingRestController controlador;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void llamarNotificacionTest() {
        String idPedido = "idPedido";
        String mail = "mail";
        String password = "password";
        ResponseEntity<String> respuestaEsperada = ResponseEntity.ok("Notification sent successfully");

        ResponseEntity<String> respuestaActual = controlador.callNotification(idPedido, mail, password);

        Assert.assertEquals(respuestaEsperada, respuestaActual);
        Mockito.verify(pedidoHandler).callPhoneNotification(idPedido, mail, password);
    }

}